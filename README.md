# Aula 02/08


 Escrevam uma função em Javascript que retorna um número aleatório entre 1 e 10 depois de 3 segundos. A função deve retornar uma Promessa que, caso o número aleatório seja menor que 5, retorna sucesso. Se o número for maior que 5, retorna erro.

Criar uma conta no GitLab (se você ainda não tiver) e subir o projeto para lá.

Vocês vão me entregar o link para o exercício no blog.
```
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function retornaNmr() {
    return new Promise(function(resolve, reject){
        setTimeout(function(){
            const aleatorio = getRandomArbitrary(1, 10);
            if(aleatorio < 5){
                resolve(aleatorio);
            }else{
                reject("Errou");
            }
          }, 3000);
          
        })
}

retornaNmr()
```